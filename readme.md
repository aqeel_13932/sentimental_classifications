تحليل عاطفي للآراء على المنتجات باللغة العربية 
====================================================================
يحتوي هذا المشروع على بيانات لما يقارب 192 الف رأي


*	**arabicNoStopWords.csv**يحتوي على كامل المعلومات (rate , review , postives , negatives , item_id) 

*	**onlyreviewsandstars.txt** يحتوي فقط على التقييم مع الرأي.

*	**NNClassifiyReviews.py** الرماز بلغة بايثون

Sentimental Classification over products reviews in Arabic language.
====================================================================
This project have data set contain 192k review.
The files in the project are : 

*	**arabicNoStopWords.csv** contain the full data (rate , review , postives , negatives , item_id) 

*	**onlyreviewsandstars.txt** contain only stars and review.

*	**NNClassifiyReviews.py** Python Code

Code main ideas :
------------------------------
*	I used [keras library](http://keras.io/ "Title") for this project

*	The module used is Multilayer Perceptron.

*	The reviews represented as bag of word.

------------------------------

Aqeel Labash

University of Tartu

Faculty of mathematics and computer science.