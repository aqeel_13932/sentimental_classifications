__author__ = 'aqeel'
'''Train and evaluate a simple MLP on the Souq.com Reviews newswire topic classification task.
GPU run command:
    THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 python examples/NNClassifiyReviews.py
CPU run command:
    python examples/NNClassifiyReviews.py
'''
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.utils import np_utils
from keras.preprocessing.text import Tokenizer
import keras.callbacks
import random
import math

#DONE
#1-1900 (depending on System Memory)
max_words = 1900
#1-inf
batch_size = 70
#1-inf
nb_epoch = 5
#Done
#SGD, RMSprop, Adagrad, Adadelta, Adam, Adamax
theoptimizer = 'Adadelta'
#DONE
#1-inf
layernodes = 128
#DONE
#0.1-0.9
thedropout =0.5
#DONE
#softmax,softplus,relu,tanh,sigmoid,hard_sigmoid,linear,
FirstActivation = 'relu'
SecondActivation='softmax'
#DONE
#mean_squared_error / mse,root_mean_squared_error / rmse,mean_absolute_error / mae,mean_absolute_percentage_error / mape
#mean_squared_logarithmic_error / msle,squared_hinge, hinge,binary_crossentropy: Also known as logloss,categorical_crossentropy: Also known as multiclass logloss. Note: using this objective requires that your labels are binary arrays of shape (nb_samples, nb_classes).
#poisson: mean of (predictions - targets * log(predictions))# cosine_proximity: the opposite (negative) of the mean cosine proximity between predictions and targets.
theloss='categorical_crossentropy'
#DONE
#modes: binary, count, tfidf, freq (best test accuracy count),(best Train Accuracy binary)
sequencemode = 'count'
#======================

results = open('results.txt','a')
results.write('\n'+str(max_words)+','+str(batch_size)+','+str(nb_epoch)+','+sequencemode+','+theoptimizer+','+str(layernodes)+','\
              +str(thedropout)+','+FirstActivation+','+SecondActivation + ','+theloss)

#Get The Information form the file
file = open('onlyreviewsandstars.txt','rb')
filelines= file.readlines()

#This way didn't work with unicode
'''
for line in filelines:
    line = line.split('#')
Get The Data (Training , Testing)
'''

'''
In This Function we get the data from the file and convert it
to training set and testing set
'''
def GetData(splitper=0.2):

    ############This way didn't work with unicode#############
    #with open('arabicNoStopWords.csv','rb') as file:
    #    reader = unicodecsv.DictReader(file,delimiter ='#')
    #    reviews = list(reader)

    #Convert The Percentage to split point
    splitper = int(math.floor(splitper * len(filelines)) + 1)

    #Shuffle the list
    random.shuffle(filelines)

    #This one used to check if there is any bad apples
    '''
    try:
        for i in range(0,len(reviews)):
            int(reviews[i]['stars'])
    except Exception as err:
        print i,reviews[i]['review']
        print err
    '''

    '''
    #this way created a problem with unicode (Guess how long to discove that )
    try:
        x_train = [s['review'] for s in reviews[:splitper]]
        y_train = [int(s['stars']) for s in reviews[:splitper]]
        x_test = [s['review'] for s in reviews[splitper:]]
        y_test = [int(s['stars']) for s in reviews[splitper:]]
    except Exception as err:
        print err.message,s['stars'],'problem here'
    '''
    #get The Informations
    traininglst = filelines[splitper:]
    testinglst = filelines[:splitper]
    x_train = []
    y_train = []
    x_test=[]
    y_test=[]
    for review in traininglst:
        x_train.append(review.split('#')[0])
        y_train.append(int(review.split('#')[1]))
    for review in testinglst:
        x_test.append(review.split('#')[0])
        y_test.append(int(review.split('#')[1]))
    return (x_train,y_train),(x_test,y_test)


print('Loading data...')

(X_train, y_train), (X_test, y_test) =GetData()
lst = []
for line in filelines:
    lst.append(line.split('#')[0])

#Number of Categories
nb_classes = np.max(y_train)+1
print(nb_classes, 'classes')

#reuters.load_data(nb_words=max_words, test_split=0.2)




print('Vectorizing sequence data...')
tokenizer = Tokenizer(nb_words=max_words)

#Provide the whole text to the tokenizer
tokenizer.fit_on_texts(lst)

#Sequence The Training and Testing Set
X_train = tokenizer.texts_to_sequences(X_train)
X_test= tokenizer.texts_to_sequences(X_test)

print(len(X_train), 'train sequences')
print(len(X_test), 'test sequences')

X_train = tokenizer.sequences_to_matrix(X_train, mode=sequencemode)
X_test = tokenizer.sequences_to_matrix(X_test, mode=sequencemode)
print('X_train shape:', X_train.shape)
print('X_test shape:', X_test.shape)


print('Convert class vector to binary class matrix (for use with categorical_crossentropy)')
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)
print('Y_train shape:', Y_train.shape)
print('Y_test shape:', Y_test.shape)

print('Building model...')
model = Sequential()
model.add(Dense(layernodes, input_shape=(max_words,)))
model.add(Activation(FirstActivation))
model.add(Dropout(thedropout))
model.add(Dense(nb_classes))
model.add(Activation(SecondActivation))
model.compile(loss=theloss, optimizer=theoptimizer)

#checkpointer = ModelCheckpoint(filepath="weights.hdf5", verbose=1, save_best_only=True,monitor='val_acc',mode='max')
losshistory= LossHistory()
history = model.fit(X_train, Y_train, nb_epoch=nb_epoch, batch_size=batch_size, verbose=1, show_accuracy=True, validation_split=0.1)
score = model.evaluate(X_test, Y_test, batch_size=batch_size, verbose=1, show_accuracy=True)
results.write(','+str(score[1])+','+str(max(history.history.get('acc'))))
print('Test score:', score[0])
print('Test accuracy:', score[1])
